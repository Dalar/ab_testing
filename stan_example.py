
# coding: utf-8

# In[ ]:


get_ipython().run_line_magic('matplotlib', 'inline')
get_ipython().run_line_magic('load_ext', 'watermark')
get_ipython().run_line_magic('watermark', "-a 'Arttu Modig' -v -m -d -t -p numpy,pandas,seaborn,matplotlib,pystan,notebook")


# In[ ]:


get_ipython().run_line_magic('load_ext', 'autoreload')
get_ipython().run_line_magic('autoreload', '2')


# In[ ]:


import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from ab_testing.analysis import analyze_pairwise_grouped_data, hpd
from ab_testing.plotting import plot_ab_test_diff_of_means, plot_ab_test_mean_posteriors
from IPython.display import display


# In[ ]:


#%env CC # might be important to set & check, if using other compilers


# In[ ]:


get_ipython().run_line_magic('pwd', '')


# In[ ]:


data = pd.read_csv("data/data_sales_grouped.csv")
data.head()


# In[ ]:


# run the most simple legacy model
results = analyze_pairwise_grouped_data(data, n_iterations=2000, stan_file="legacy_updated.stan")


# In[ ]:


results.keys()


# In[ ]:


fig, ax = plt.subplots()
ax = plot_ab_test_mean_posteriors(mean_a=results['lognormal_mean_a'],
                             mean_b=results['lognormal_mean_b'],
                             conversion_a=np.mean(results['conversion_a']),
                             conversion_b=np.mean(results['conversion_b']),
                             N=results['samples_total'],
                             iter=results['iter_total'],
                             ax=ax)


# In[ ]:


fig, ax = plt.subplots()
ax = plot_ab_test_diff_of_means(mean_a=results['lognormal_mean_a'],
                                mean_b=results['lognormal_mean_b'])


# In[ ]:


# effective result per purchase
fig, ax = plt.subplots()
ax = plot_ab_test_diff_of_means(mean_a=results['conversion_a'] * results['lognormal_mean_a'],
                           mean_b=results['conversion_b'] * results['lognormal_mean_b'],
                           title="Effective difference per session",
                           xlabel="€")


# In[ ]:


# Simple stopping rule. See the basic idea here:
# https://www.smartly.io/blog/statistical-significance-for-humans-automated-statistical-significance-calculator-for-a/b-testing

smallest_interesting_difference = 0.05  # 5 snt
alpha = 0.05

def evaluate_HDI(HDI, precision):
    if np.diff(HDI) > 2 * precision:
        print("Continue testing. ({a:.2f} > {b})".format(a=np.diff(HDI)[0],
                                                    b=2*precision))
    else:
        if np.prod(HDI) > 0:
            better, worse = ("A", "B") if HDI[0] > 0 else ("B", "A")
            print('Stop the test: {} is better than {}.'.format(better, worse))
        else:
            print('Stop the test: No difference found.')

diff = (results['conversion_b'] * results['lognormal_mean_b']
        - results['conversion_a'] * results['lognormal_mean_a'])
x = np.sort(diff)
a, b = hpd(x, alpha)

evaluate_HDI((a,b), 0.5 * smallest_interesting_difference)

