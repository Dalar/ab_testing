functions {
  real lognormal_mean(real mu, real sigma) {
    return exp(mu + 0.5 * sigma * sigma);
  }
  real lognormal_sd(real mu, real sigma) {
    real a;
    real b;
    a = exp(sigma * sigma) - 1;
    b = exp(2 * mu + sigma * sigma);
    return sqrt(a * b); // sqrt of variance
  }
}
data {
  int<lower=0> N; // number of cases
  int<lower=0> J; // number of groups
  real<lower=0> y[N]; // observed outcome i.e. revenue
  int<lower=0, upper=1> iszero[N]; // indicates negative outcomes
  int<lower=1> id[N]; // group number for each case
  real<lower=0> mean_log_y; // mean of log-positive outcomes
  real<lower=0> sd_log_y; // sd of log-positive outcomes
}
parameters {
  vector<lower=0, upper=1>[J] theta; // chance of success per test group, i.e. conversion rates
  real<lower=0, upper=1> phi; // population chance of success
  real<lower=1> kappa; // population concentration
  vector<lower=0>[J] mu; // mu for lognormal distribution per group
  vector<lower=0>[J] sigma; // sigma for lognormal distribution per group
}
model {
  // Priors for Bernoulli:
  // phi ~ beta(1, 1); // hyperprior (uniform, could drop, change to beta(2,2)?)
  phi ~ beta(2, 2);
  kappa ~ pareto(1, 1.5); // hyperprior (requires that kappa > 1st Pareto parameter)
  theta ~ beta(kappa * phi, kappa * (1 - phi)); // prior

  // The following empirical priors for Log-Normal are from Kruschke's
  // example. Note that I use pooled data:
  mu ~ normal(mean_log_y, 0.001 / (sd_log_y * sd_log_y));
  sigma ~ uniform(0.001 * sd_log_y, 1000 * sd_log_y);
  // mu ~ normal(2, 10); // legacy prior for log-normal

  // Likelihood sampling statements:
  for (n in 1:N) {
    iszero[n] ~ bernoulli(theta[id[n]]);
    if (y[n] > 0) {
      y[n] ~ lognormal(mu[id[n]], sigma[id[n]]);
    }
  }
}
generated quantities {
  vector[J] revenue_mean;
  vector[J] revenue_std;

  for (j in 1:J) {
    revenue_mean[j] = lognormal_mean(mu[j], sigma[j]);
    revenue_std[j] = lognormal_sd(mu[j], sigma[j]);
  }
}
