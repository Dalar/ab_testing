functions {
  real lognormal_mean(real mu, real sigma) {
    return exp(mu + 0.5 * sigma * sigma);
  }
  real lognormal_sd(real mu, real sigma) {
    real a;
    real b;
    a = exp(sigma * sigma) - 1;
    b = exp(2 * mu + sigma * sigma);
    return sqrt(a * b); // sqrt of variance
  }
}
data {
  int<lower=0> N;
  real<lower=0> y[N]; // observed outcome i.e. revenue
  int<lower=1, upper=2> x[N];
  int<lower=1> count[N]; // count of each unique outcome
}
parameters {
  vector<lower=0, upper=1>[2] theta; // bernoulli prob

  // empirical priors for Log-Normal(mu, sigma)
  real<lower=0> mean_log_y; // mean of log-positive outcomes
  real<lower=0> sd_log_y; // sd of log-positive outcomes

  vector<lower=0>[2] mu_sigma; // scale for mu ~ Normal
  vector<lower=0>[2] sigma_sigma; // scale for sigma ~ Normal
  vector<lower=0>[2] mu; // lognormal mu
  vector<lower=0, upper=10>[2] sigma; // lognormal sigma
}
model {
  mu_sigma ~ student_t(4, 0, 1); // hyperprior; param #3 = mean of half-t-dist.
  sigma_sigma ~ student_t(4, 0, 0.05); // hyperprior
  mu ~ normal(mean_log_y, mu_sigma);
  sigma ~ normal(sd_log_y, sigma_sigma);

  for (n in 1:N) {
    if (y[n] > 0)
      target += count[n] * (bernoulli_lpmf(1 | theta[x[n]])
                        + lognormal_lpdf(y[n] | mu[x[n]], sigma[x[n]]));
    else
      target += count[n] * bernoulli_lpmf(0 | theta[x[n]]);
  }
}
generated quantities {
  real lognormal_mean_a;
  real lognormal_mean_b;

  lognormal_mean_a = lognormal_mean(mu[1], sigma[1]);
  lognormal_mean_b = lognormal_mean(mu[2], sigma[2]);
}
