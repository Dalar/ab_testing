import os.path
from pkg_resources import get_distribution, DistributionNotFound

# For package version good practise, see:
# http://stackoverflow.com/questions/17583443/what-is-the-correct-way-to-share-package-version-with-setup-py-and-the-package
try:
    _dist = get_distribution('ab_testing')
    # normalize case for Windows systems
    dist_loc = os.path.normcase(_dist.location)
    here = os.path.normcase(__file__)
    if not here.startswith(os.path.join(dist_loc, 'ab_testing')):
        # not installed, but there is another version that *is*
        raise DistributionNotFound
except DistributionNotFound:
    __version__ = ('Distribution not found from PyPi. Please install this '
                   'package from source (either with `pip install '
                   './ab_testing` or `python setup.py`).')
else:
    __version__ = _dist.version
