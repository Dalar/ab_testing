import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from .analysis import hpd


def plot_pairwise_test_results(results, figsize=None):
    if figsize is None:
        default = mpl.rcParams['figure.figsize']
        fig, axes = plt.subplots(2, 2, figsize=(1.9*default[0], 2*default[1]))
    else:
        fig, axes = plt.subplots(2, 2, figsize=figsize)

    axes = axes.ravel()

    plot_ab_test_mean_posteriors(mean_a=results['lognormal_mean_a'],
                                 mean_b=results['lognormal_mean_b'],
                                 conversion_a=np.mean(results['conversion_a']),
                                 conversion_b=np.mean(results['conversion_b']),
                                 N=results['samples_total'],
                                 iter=results['iter_total'],
                                 ax=axes[0])

    plot_ab_test_diff_of_means(mean_a=results['lognormal_mean_a'],
                               mean_b=results['lognormal_mean_b'],
                               alpha=0.05,
                               ax=axes[1])

    plot_ab_test_mean_posteriors(mean_a=results['conversion_a']*results['lognormal_mean_a'],
                                 mean_b=results['conversion_b']*results['lognormal_mean_b'],
                                 conversion_a=np.mean(results['conversion_a']),
                                 conversion_b=np.mean(results['conversion_b']),
                                 N=results['samples_total'],
                                 iter=results['iter_total'],
                                 title="Posterior samples of effective sales mean",
                                 xlabel="effective mean",
                                 ax=axes[2])

    plot_ab_test_diff_of_means(mean_a=results['conversion_a']*results['lognormal_mean_a'],
                               mean_b=results['conversion_b']*results['lognormal_mean_b'],
                               alpha=0.05, xlabel="$\Delta$ [effective mean]",
                               ax=axes[3])

    plt.tight_layout()


def plot_ab_test_mean_posteriors(mean_a, mean_b, conversion_a, conversion_b,
                                 N, iter, title="Posterior samples of log-normal revenue mean",
                                 xlabel="revenue mean", ax=None):
    """Plot A/B test mean posteriors with conversion information."""
    if ax is None:
        ax = plt.gca()
    ax.hist(mean_b, bins=100, alpha=0.85, label="test")
    ax.hist(mean_a, bins=100, alpha=0.65, label="ctrl")
    ax.set_title(title)
    ax.set_xlabel(xlabel)
    ax.legend()
    # fine-tune
    y_max_orig = ax.get_ylim()[1]
    ax.set_ylim([0, 1.25 * y_max_orig])
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.spines["left"].set_visible(False)
    ax.yaxis.set_visible(False)
    # info text
    ax.text(0, 0.99, 'Test group conversion: {k1:0.1f} %\n'
                     'Control group conversion: {k0:0.1f} %\n'
                     'Test sample size: {N}\nMCMC iterations: {i}'
            .format(k1=100 * conversion_b, k0=100 * conversion_a, N=N, i=iter),
            fontsize=11, horizontalalignment='left', verticalalignment='top',
            transform=ax.transAxes)
    # mean lines and their annotation
    ax.axvline(x=mean_b.mean(), ymax=1 / 1.3, color='black',
               linestyle='dashed', linewidth=1)
    ax.axvline(x=mean_a.mean(), ymax=1 / 1.3, color='black',
               linestyle='dashed', linewidth=1)
    height = ax.get_ylim()[1] - ax.get_ylim()[0]
    width = ax.get_xlim()[1] - ax.get_xlim()[0]
    ax.text(mean_a.mean() + 0.02 * width, 0.15 * height,
            '{0:.4f}'.format(mean_a.mean()), color='black', fontsize=11)
    ax.text(mean_b.mean() + 0.02 * width, 0.10 * height,
            '{0:.4f}'.format(mean_b.mean()), color='black', fontsize=11)
    return ax


def plot_ab_test_diff_of_means(mean_a, mean_b, alpha=0.05,
                               title="Posterior samples of the difference of means",
                               xlabel="$\Delta$ [revenue mean]", ax=None):
    """Plot A/B test difference of posterior means.

    The plot includes a Highest Density Interval (HDI),
    with a default credibility of 95 % of posterior mass."""

    diff = mean_b - mean_a
    x = np.sort(diff)
    a, b = hpd(x, alpha)

    if ax is None:
        ax = plt.gca()
    ax.hist(diff, bins=100, color='lightgrey')
    ax.axvline(0, color='black', linewidth=1)
    ax.axvline(np.mean(x), color='black', linewidth=1, linestyle='--')
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.spines["left"].set_visible(False)
    ax.yaxis.set_visible(False)
    # color HPD patches
    x_patches = list(
        map(lambda x: x.get_x() + 0.5 * x.get_width(), ax.patches))
    idx_patches_between = np.where((x_patches >= a) & (x_patches <= b))[0]
    patches_between = list(ax.patches[i] for i in idx_patches_between)
    for p in patches_between:
        p.set_facecolor('c')
    # add info labels
    ax.set_xlabel(xlabel)
    legend_patch = mpatches.Patch(color='c', label='{:.0f} % HDI'.format(
        100 * (1 - alpha)))
    ax.legend(handles=[legend_patch])
    ax.set_title(title)
    height = ax.get_ylim()[1] - ax.get_ylim()[0]
    width = ax.get_xlim()[1] - ax.get_xlim()[0]
    ax.text(np.mean(x) + 0.02 * width, 0.1 * height,
            '{0:.4f}'.format(np.mean(x)), color='black', fontsize=11)
    return ax
