import os
import numpy as np
import pystan
import multiprocessing

PACKAGE_DIR = os.path.dirname(os.path.abspath(__file__))
STAN_DIR = os.path.join(PACKAGE_DIR, 'stan_models')
CPU_COUNT = multiprocessing.cpu_count()


def analyze_pairwise_grouped_data(data, n_iterations=2000, n_chains=CPU_COUNT-1,
                                  n_jobs=CPU_COUNT-1, compiler='default',
                                  stan_file="pairwise.stan",
                                  value_col="revenue", count_col="count",
                                  label_col="label"):
    """Fit A/B/C/.../N model with Stan and extract the results.

    The data should contain grouped transaction data by revenue value. Use
    the following default column names (or change them via arguments):
    - "revenue" (int or float), revenue of each transaction revenue group
    - "count" (int), number of transactions in each revenue group
    - "label" (int), test group label number starting from 1

    Arguments:
        data (:obj:`DataFrame`): A Pandas DataFrame of grouped revenue data.
        n_iterations (int, optional): The number of MCMC iterations per chain.
            Default = 2000.
        n_chains (int, optional): The number of MCMC chains. Default is the
            number of available CPU cores minus 1.
        n_jobs (int, optional): The number of parallelized MCMC processes.
            Default is the number of available CPU cores minus 1.
        compiler ({'gcc','default'}, optional): Choose the Stan C++ compiler,
            options are the system default or `gcc` (if it's installed!). MacOS
            might have issues with the default compiler, so in case of a
            possible segfault, try `gcc`.
        stan_file (str, optional): The stan model file (which are included).
        value_col (str, optional): The name of the data value column, by which
            the data has been grouped. The default is "revenue".
        count_col (str, optional): The name of the group counts column. The
            default is "count".
        label_col (str, optional): The name of the test group label column. The
            default is "label".

    Returns:
        Results from the Stan model of A/B/C/.../N testing.
    """
    if compiler == 'gcc':
        os.environ["CC"] = "gcc"
        os.environ["CXX"] = "g++"
        os.environ["CXXFLAGS"] = "-O3 -mtune=native -march=native -Wno-unused-variable -Wno-unused-function"

    iterations_total = n_iterations * n_chains
    samples_total = data[count_col].sum()

    # some empirical estimates for the Stan model
    y = data[value_col]
    y = y[y > 0]
    mean_log_y = np.mean(np.log(y))
    sd_log_y = np.std(np.log(y))

    # format the date for PyStan
    model_data = {"N": data.shape[0],
                  "y": data[value_col].astype(float).values,
                  "x": data[label_col].astype(int).values,
                  "count": data[count_col].astype(int).values,
                  "mean_log_y": mean_log_y,
                  "sd_log_y": sd_log_y}

    # construct the model
    stan_file_path = os.path.join(STAN_DIR, stan_file)
    sm = pystan.StanModel(file=stan_file_path, verbose=False)

    # fit the model
    fit = sm.sampling(data=model_data, iter=n_iterations, chains=n_chains,
                      n_jobs=n_jobs, verbose=False,
                      control={'adapt_delta': 0.99, 'max_treedepth': 10})

    # extract results
    theta_a = fit.extract()['theta'][:, 0]
    theta_b = fit.extract()['theta'][:, 1]
    lognormal_mean_a = fit.extract()['lognormal_mean_a']
    lognormal_mean_b = fit.extract()['lognormal_mean_b']
    summary = fit.summary()

    return {'conversion_a': theta_a, 'conversion_b': theta_b,
            'lognormal_mean_a': lognormal_mean_a,
            'lognormal_mean_b': lognormal_mean_b,
            'samples_total': samples_total, 'summary': summary,
            'iter_total': iterations_total}


def analyze_hierarchical_grouped_data(data, n_iterations=10000, n_chains=CPU_COUNT-1,
                                 n_jobs=CPU_COUNT-1, compiler='default'):
    """Fit A/B/C/.../N model with Stan and extract the results.

    The data should contain grouped transaction data by revenue value. Use
    the following column names:
    - "revenue" (int or float), revenue of each transaction revenue group
    - "count" (int), number of transactions in each revenue group
    - "label" (int), test group label number starting from 1

    Arguments:
        data (:obj:`DataFrame`): A Pandas DataFrame of grouped revenue data.
        n_iterations (int, optional): The number of MCMC iterations per chain.
            Default = 10000.
        n_chains (int, optional): The number of MCMC chains. Default is the
            number of available CPU cores minus 1.
        n_jobs (int, optional): The number of parallelized MCMC processes.
            Default is the number of available CPU cores minus 1.
        compiler ({'gcc','default'}, optional): Choose the Stan C++ compiler,
            options are the system default or `gcc` (if it's installed!). MacOS
            might have issues with the default compiler, so in case of a
            possible segfault, try `gcc`.

    Returns:
        Results from the Stan model of A/B/C/.../N testing.
    """
    if compiler == 'gcc':
        os.environ["CC"] = "gcc"
        os.environ["CXX"] = "g++"
        os.environ["CXXFLAGS"] = "-O3 -mtune=native -march=native -Wno-unused-variable -Wno-unused-function"

    iterations_total = n_iterations * n_chains
    samples_total = data['count'].sum()

    # some empirical estimates for the Stan model
    rev = data['revenue']
    rev = rev[rev > 0]
    mean_log_y = np.mean(np.log(rev))
    sd_log_y = np.std(np.log(rev))

    # massage the data for PyStan
    model_data = {"N": data.shape[0],
                  "J": data['label'].nunique(),
                  "y": data['revenue'].astype(float).values,
                  "id": data['label'].astype(int).values,
                  # id should start from 1!
                  "count": data['count'].astype(int).values,
                  "mean_log_y": mean_log_y,
                  "sd_log_y": sd_log_y}

    # construct the model
    stan_file = os.path.join(STAN_DIR, 'hier_grouped.stan')
    sm = pystan.StanModel(file=stan_file, verbose=False)

    # fit the model
    fit_hier = sm.sampling(data=model_data, iter=n_iterations, chains=n_chains,
                           n_jobs=n_jobs, verbose=False,
                           control={'adapt_delta': 0.99, 'max_treedepth': 10})

    # extract results
    revenue_mean = fit_hier.extract()['revenue_mean']
    revenue_std = fit_hier.extract()['revenue_std']
    conversion = fit_hier.extract()['theta']
    summary = fit_hier.summary()

    return {'revenue_mean': revenue_mean, 'revenue_std': revenue_std,
            'conversion': conversion, 'iter_total': iterations_total,
            'samples_total': samples_total, 'summary': summary}


def hpd(x, alpha=0.05, transform=lambda x: x):
    """Calculate highest posterior density (HPD) of array for given alpha.
    The HPD is the minimum width Bayesian credible interval (BCI).

    This method was taken from PyMC3 library.

    :Arguments:
      x : Numpy array
          An array containing MCMC samples
      alpha : float
          Desired probability of type I error (defaults to 0.05)
      transform : callable
          Function to transform data (defaults to identity)
    """
    # Make a copy of trace
    x = transform(x.copy())

    # For multivariate node
    if x.ndim > 1:
        raise NotImplementedError
    else:
        # Sort univariate node
        x = np.sort(x)

        n = len(x)
        cred_mass = 1.0 - alpha

        interval_idx_inc = int(np.floor(cred_mass * n))
        n_intervals = n - interval_idx_inc
        interval_width = x[interval_idx_inc:] - x[:n_intervals]

        if len(interval_width) == 0:
            raise ValueError('Too few elements for interval calculation')

        min_idx = np.argmin(interval_width)
        hdi_min = x[min_idx]
        hdi_max = x[min_idx + interval_idx_inc]
        return hdi_min, hdi_max
