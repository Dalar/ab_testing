# A/B (or A/B/C/.../N) test data analysis

## Requirements

* numpy
* cython
* pandas
* jupyter (optional for notebook files)
* pystan (see installation notes below)

Anaconda environment is recommended (especially for Windows!), but not a requirement.

## Installing

First you should install PyStan, and the easiest recommended method is to use `pip`:

```pip install pystan```

Then you can clone this repo and install it with:

```pip install ab_testing```
(use the repo root where `setup.py` is located)

After this, you should try running the example notebook `stan_example.ipynb`. If the model _segfaults_ (i.e. iPython kernel dies), you should try a different C++ compiler.

MacOS + Anaconda installation may have issues with C++ libraries. One possible solution is to change the default compiler from `clang` to `gcc`. When using Anaconda environment, you should install `gcc` with `conda install gcc`. Then you should set the **compiler** parameter to **gcc** when running the data analysis functions, e.g. `analyze_grouped_revenue_data(data, n_iterations=10000, n_chains=8, compiler='gcc')`. This will set the environment variables to be the following before compiling the Stan model:
```
os.environ["CC"] = "gcc"
os.environ["CXX"] = "g++"
os.environ["CXXFLAGS"] = "-O3 -mtune=native -march=native -Wno-unused-variable -Wno-unused-function"
```

To be safe, it's also possible to compile and install PyStan from source using `gcc`:
```
export CC="gcc"
export CXX="g++"
export CXXFLAGS="-O3 -mtune=native -march=native -Wno-unused-variable -Wno-unused-function"
git clone --recursive https://github.com/stan-dev/pystan.git
cd pystan
python setup.py install
```

The compiler flags were suggested in RStan documentation (not sure how useful they are with PyStan):
https://github.com/stan-dev/rstan/wiki/Installing-RStan-on-Mac-or-Linux

## Methods and modules

Analysis functions are contained in the `analysis.py` module. Plotting functions are separated to `plotting.py` module.
