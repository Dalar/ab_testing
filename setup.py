#!/usr/bin/env python
import os
from distutils.core import setup


def read(filename):
    return open(os.path.join(os.path.dirname(__file__), filename)).read()


setup(
    name='ab_testing',
    version='0.0.1.dev2',
    description="A/B testing toolbox",
    author="Arttu Modig",
    author_email="arttu.modig@veikkaus.fi",
    packages=['ab_testing'],
    license="© Veikkaus",
    long_description=read('README.md'),
    install_requires=[
        "numpy",
        "pandas",
        "cython",
        "pystan",
    ],
    package_data={
        "ab_testing": [
            "../README.md",
            "../MANIFEST.in",
            "../LICENSE",
            "stan_models/hier.stan",
            "stan_models/hier_grouped.stan",
            "legacy_updated.stan",
            "pairwise.stan"
        ]
    }
)
